from twisted.internet.defer import inlineCallbacks

from yombo.core.exceptions import YomboWarning
from yombo.core.log import get_logger
from yombo.lib.webinterface.auth import require_auth

logger = get_logger("modules.devicebridge.web_routes")

def module_presence_routes(webapp):
    """
    Adds routes to the webinterface module.

    :param webapp: A pointer to the webapp, it's used to setup routes.
    :return:
    """
    with webapp.subroute("/") as webapp:

        def root_breadcrumb(webinterface, request):
            webinterface.add_breadcrumb(request, "/", "Home")
            webinterface.add_breadcrumb(request, "/tools/module_presence", "Presence")

        def view_on_master(webinterface, request):
            page = webinterface.webapp.templates.get_template('modules/presence/we/view_on_master.html')
            root_breadcrumb(webinterface, request)
            return page.render(alerts=webinterface.get_alerts())

        @webapp.route("/tools/module_presence", methods=['GET'])
        @require_auth()
        def page_tools_module_presence_get(webinterface, request, session):
            local_gateway = webinterface._Gateways.local
            if local_gateway.is_master is False:
                return view_on_master(webinterface, request)

            presence = webinterface._Modules['Presence']
            page = webinterface.webapp.templates.get_template('modules/presence/web/index.html')
            root_breadcrumb(webinterface, request)
            return page.render(alerts=webinterface.get_alerts(),
                               trackables=presence.presence_data,
                               sub_modules=presence.sub_modules,
                               )

        @webapp.route('/tools/module_presence/add', methods=['GET'])
        @require_auth()
        # @inlineCallbacks
        def page_tools_module_presence_add_get(webinterface, request, session):
            local_gateway = webinterface._Gateways.local
            if local_gateway.is_master is False:
                return view_on_master(webinterface, request)

            presence = webinterface._Modules['Presence']
            data = {
                'label': webinterface.request_get_default(request, 'label', ""),
                'machine_label': webinterface.request_get_default(request, 'machine_label', ""),
                'description': webinterface.request_get_default(request, 'description', ""),
            }
            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/tools/module_presence/add", "Add")
            return page_tools_module_presence_form(webinterface, request, session, presence, 'add', data,
                                                "Add Trackable")

        @webapp.route('/tools/module_presence/add', methods=['POST'])
        @require_auth()
        @inlineCallbacks
        def page_tools_module_presence_add_post(webinterface, request, session):
            local_gateway = webinterface._Gateways.local
            if local_gateway.is_master is False:
                return view_on_master(webinterface, request)

            presence = webinterface._Modules['Presence']
            print("aaaaaa")
            new_node = {
                'label': webinterface.request_get_default(request, 'label', ""),
                'machine_label': webinterface.request_get_default(request, 'machine_label', ""),
                'description': webinterface.request_get_default(request, 'description', ""),
                'data': {
                    'last_location': {},
                    'modules': {},
                }
            }

            print("aaaaaa2")
            add_trackable_results = yield presence.add_node(new_node)

            print("aaaaaa3: %s" % add_trackable_results)
            if add_trackable_results['status'] == 'failed':
                print("bbb")
                webinterface.add_alert(add_trackable_results['apimsghtml'], 'warning')
                print("bbb")
                return page_tools_module_presence_form(webinterface, request, session, presence, 'add', new_node,
                                                       "Add Trackable")
            print("aaaaaa4")

            msg = {
                'header': 'Trackable Added',
                'label': 'Device typ added successfully',
                'description': '<p>The trackable has been added. If you have requested this trackable to be made public, please allow a few days for Yombo review.</p><p>Continue to <a href="/tools/module_presence">trackables index</a> or <a href="/devtools/tools/module_presence/%s/details">view the new trackable</a>.</p>' %
                               add_trackable_results['device_type_id'],
            }
            print("aaaaaa5")

            page = webinterface.get_template(request, webinterface.wi_dir + '/pages/display_notice.html')
            print("aaaaaa6")
            root_breadcrumb(webinterface, request)
            print("aaaaa7")
            webinterface.add_breadcrumb(request, "/tools/module_presence", "Trackables")
            print("aaaaaa8")
            webinterface.add_breadcrumb(request, "/devtools/tools/module_presence/add", "Add")
            print("aaaaa9")
            return page.render(alerts=webinterface.get_alerts(),
                                    msg=msg,
                                    )

        @webapp.route('/tools/module_presence/<string:device_type_id>/edit', methods=['GET'])
        @require_auth()
        @inlineCallbacks
        def page_tools_module_presence_edit_get(webinterface, request, session, device_type_id):
            local_gateway = webinterface._Gateways.local
            if local_gateway.is_master is False:
                return view_on_master(webinterface, request)

            presence = webinterface._Modules['Presence']
            try:
                add_trackable_results = yield webinterface._YomboAPI.request('GET',
                                                                             '/v1/device_type/%s' % device_type_id,
                                                                             session=session['yomboapi_session'])
            except YomboWarning as e:
                webinterface.add_alert(e.html_message, 'warning')
                return webinterface.redirect(request, '/tools/module_presence')

            root_breadcrumb(webinterface, request)
            webinterface.add_breadcrumb(request, "/tools/module_presence", "Trackables")
            webinterface.add_breadcrumb(request, "/devtools/tools/module_presence/%s/details" % device_type_id,
                                        add_trackable_results['data']['label'])
            webinterface.add_breadcrumb(request, "/devtools/tools/module_presence/%s/edit" % device_type_id, "Edit")

            return page_tools_module_presence_form(webinterface, request, session, presence, 'edit', data,
                                                "Edit Trackable: %s" % add_trackable_results['data']['label'])

        @webapp.route('/tools/module_presence/<string:device_type_id>/edit', methods=['POST'])
        @require_auth()
        @inlineCallbacks
        def page_tools_module_presence_edit_post(webinterface, request, session, device_type_id):
            local_gateway = webinterface._Gateways.local
            if local_gateway.is_master is False:
                return view_on_master(webinterface, request)

            presence = webinterface._Modules['Presence']
            data = {
                'label': webinterface.request_get_default(request, 'label', ""),
                'machine_label': webinterface.request_get_default(request, 'machine_label', ""),
                'description': webinterface.request_get_default(request, 'description', ""),
            }

            add_trackable_results = yield presence.add_node(data)

            if add_trackable_results['status'] == 'failed':
                webinterface.add_alert(add_trackable_results['apimsghtml'], 'warning')
                return page_tools_module_presence_form(webinterface, request, session, presence, 'edit', data,
                                                       "Edit Trackable")

            msg = {
                'header': 'Trackable Updated',
                'label': 'Device typ updated successfully',
                'description': '<p>The trackable has been updated. If you have requested this trackable to be made public, please allow a few days for Yombo review.</p><p>Continue to <a href="/tools/module_presence">trackables index</a> or <a href="/devtools/tools/module_presence/%s/details">view the new trackable</a>.</p>' %
                               add_trackable_results['device_type_id'],
            }

            try:
                add_trackable_results = yield webinterface._YomboAPI.request('GET',
                                                                             '/v1/device_type/%s' % device_type_id,
                                                                             session=session['yomboapi_session'])
            except YomboWarning as e:
                webinterface.add_breadcrumb(request, "/tools/module_presence", "Trackables", True)
                webinterface.add_alert(e.html_message, 'warning')
                return webinterface.redirect(request, '/tools/module_presence')

            page = webinterface.get_template(request, webinterface.wi_dir + '/pages/display_notice.html')
            root_breadcrumb(webinterface, request)

            webinterface.add_breadcrumb(request, "/tools/module_presence", "Trackables")
            webinterface.add_breadcrumb(request, "/devtools/tools/module_presence/%s/details" % device_type_id,
                                        add_trackable_results['data']['label'])
            webinterface.add_breadcrumb(request, "/devtools/tools/module_presence/%s/enable" % device_type_id,
                                        "Enable")

            return page.render(alerts=webinterface.get_alerts(),
                                    msg=msg,
                                    )

        def page_tools_module_presence_form(webinterface, request, session, presence, action_type, trackable,
                                            header_label):
            page = webinterface.get_template(request, 'modules/presence/web/form.html')
            return page.render(alerts=webinterface.get_alerts(),
                               header_label=header_label,
                               trackable=trackable,
                               action_type=action_type,
                               )
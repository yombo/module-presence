#This file was created by Yombo for use with Yombo Gateway automation
#software. Details can be found at https://yombo.net
"""
Presence Support
================

Provides presence to Yombo gateway. This module only provides a framework for
other modules to extend.

License
=======

See LICENSE.md for full license and attribution information.

The Yombo team and other contributors hopes that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
more details.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: 2012-2017 Yombo
:license: YRPL 1.6
"""

# Import python libraries
from collections import OrderedDict
import json
from time import time

from twisted.internet.defer import inlineCallbacks

from yombo.classes.fuzzysearch import FuzzySearch
from yombo.core.exceptions import YomboWarning
from yombo.core.log import get_logger
from yombo.core.module import YomboModule
from yombo.utils import global_invoke_all

from yombo.modules.presence.web_routes import module_presence_routes

logger = get_logger("modules.presence")


class Presence(YomboModule):
    """
    Tracks occupancy and presence of people in a location and/or area.
    """
    def _init_(self, **kwargs):
        self.gwid = self._Gateways.local_id
        self.sub_modules = OrderedDict()  # tracks sub_modules modules that can report presence status

        self.presence_by_id = {}  # Map of available presence_ids (aka node_ids) to names
        self.presence_by_label = FuzzySearch({})  # reverse mapping of person name to presence_id

        # self.presence_history = yield self._SQLDict.get(self, 'history')  # tracks the current presence of people
        self.presence_data = {}  # tracks the current presence of objects (or people)
        try:
            self.upload_presence = self._module_variables_cached['upload_presence']['values'][0]
        except Exception as e:
            self.upload_presence = True

    @inlineCallbacks
    def _load_(self, **kwargs):

        results = yield global_invoke_all('presence_modules', called_by=self)
        temp = {}
        for component_name, data in results.items():
            temp[data['priority']] = {'name': component_name, 'module': data['module']}
        sub_modules = OrderedDict(sorted(temp.items()))

        presense_fields = {}
        for priority, data in sub_modules.items():
            self.sub_modules[data['module']._FullName] = data
            try:
                pass
            except Exception as e:
                pass

        print("presence sub modules: %s" % self.sub_modules)

        #
        nodes = self._Nodes.search_advanced({'node_type': 'module_presence_objects'})["values"]
        for node_id, node in nodes.items():
            self.import_node(node)

    def _webinterface_add_routes_(self, **kwargs):
        """
        Adds a configuration block to the web interface. This allows users to view their nest account for
        thermostat ID's which they c--an copy to the device setup page.
        :param kwargs:
        :return:
        """
        return {
            'nav_side': [
                {
                    'label1': 'Presence',
                    'label2': 'Presence',
                    'priority1': 500,
                    'priority2': 100,
                    'icon': 'fa fa-eye fa-fw',
                    'url': '/tools/module_presence',
                    'tooltip': '',
                    'opmode': 'run',
                },
            ],
            'routes': [
                module_presence_routes,
            ],
        }

    def import_node(self, node):
        self.presence_data[node.node_id] = node.data  # current, history
        if 'current' not in self.presence_data[node.node_id]:
            self.presence_data[node.node_id]['current'] = self.blank_presence()
        if 'updates' not in self.presence_data[node.node_id]:  # tracks a stream of updates from sources
            self.presence_data[node.node_id]['updates'] = self.blank_presence()
        if 'history' not in self.presence_data[node.node_id]:
            self.presence_data[node.node_id]['history'] = {}
            blank = self.blank_presence()
            blank['source'] = self._FullName
            blank['gwid'] = self.gwid
            self.presence_data[node.node_id]['history'][int(time())] = blank

        self.presence_by_id[node.node_id] = node.label
        self.presence_by_label[node.label] = node.node_id

    @inlineCallbacks
    def add_node(self, data):
        data['node_type'] = 'module_presence_objects'
        data['destination'] = 'gw'
        results = yield self._Nodes.add_node(data)
        return results

    @inlineCallbacks
    def edit_node(self, node_id, data):
        results = yield self._Nodes.edit_node(node_id, data)
        # print("device bridge edit node results: %s" % results)
        return results

    @inlineCallbacks
    def delete_node(self, node_id):
        results = yield self._Nodes.delete_node(node_id)
        # print("devbice bridge delete node results: %s" % results)
        return results

    def update_presence(self, source_module, presence_id, presence_info, **kwargs):
        """
        Sets the presence and location of a person. This method should only be called by child modules.

        If a presence child module doesn't know the status of a person, it should simply set_presence with 'valid'
        being None. This will remove the past presence information from the calculation of location. This will
        allow modules with lower priority to fill in the gap. For example, if the bluetooth presence module lost bluetooth
        signal, but the wifi presence module see something, the wifi presence module can provide more coarse grained
        data. Other modules can include GPS presense modules for exact latitude and longitude location.

        :param source_module: Calling module's reference to itself.
        :param presence_id: Presence id being updated.
        :param presence_info: blank_presence() plus any data.
        :param valid: If false, make data from source_module for the presence_id as invalid.
        :param kwargs: Unused, for future.

        :return:
        """
        source_name = source_module._FullName
        if source_name not in self.sub_modules:
            raise YomboWarning('Unknown module as source, cannot set presence: %s' % source_name)

        location_id = presence_info['location_id']
        if location_id is None:
            raise YomboWarning('Presence updates require a location.')
        location = self._Locations.get(location_id, 'location')

        area_id = presence_info['area_id']
        if area_id is None:
            raise YomboWarning('Presence updates require an area.')
        area = self._Locations.get(area_id, 'area')

        gwid = presence_info['gwid']
        if area_id is None:
            raise YomboWarning('Presence updates require a gwid.')
        area = self._Gateways.get(gwid)

        try:
            presence_id = self.get_presence_id(presence_id)
        except YomboWarning as e:
            logger.warn("Cannot find device for presence_id: %s"  % presence_id)
            return

        if source_name not in self.presence_data[presence_id]['history']:
            self.presence_data[presence_id]['history'] = {}

            self.presence_data[presence_id]['history'][source_name] = presence_info

        self.calculate_presence_locations(presence_id)
    #
    # def add_presence(self, info):
    #     data = json.dumps({
    #         'location': None,
    #         'area': None,
    #         'latitude': None,
    #         'elevation': None,
    #         'speed': None,
    #         'direction': None,
    #         'time': time(),
    #         'blank': True,
    #     })
    #
    #     new_presence = {
    #         'node_type': 'module_presence_objects',
    #         'label': info['label'],
    #         'machine_label': info['machine_label'],
    #         'always_load': 1,
    #         'destination': 'any',
    #         'data': data,
    #         'data_content_type': json,
    #         'status': 1,
    #     }
    #     results = self._Nodes.add_node(new_presence)
    #     if results['status'] == 'success':
    #         presence_id = results['node_id']
    #         self.presence_by_id[presence_id] = info['label']
    #         self.presence_by_name[info['label']] = presence_id
    #         self.presence_data[presence_id] = data
    #
    #     return results

    def blank_presence(self):
        return {
                'presence_id': None,
                'location': None,
                'area': None,
                'latitude': None,
                'longitude': None,
                'elevation': None,
                'speed': None,
                'direction': None,
                'time': time(),
                'valid': True,
                'source': None,
                'gwid': None,
                'human_location': "Unknown",
            }

    def calculate_presence_locations(self, presence_id = None):
        """
        Updates presence information based on priority of modules and what information is available.

        If supplied a presence_id, only updates that one.
        :return:
        """

        if presence_id is not None:
            if presence_id is not None:
                presence_id = self.get_presence_id(presence_id)

            if presence_id not in self.presence_data:
                raise YomboWarning("requested presence_id is not valid.")
            self._calculate_presence_single(presence_id)

        else:
            for presence_id in self.presence_by_id:
                if presence_id in self.presence_data:
                    self._calculate_presence_single(presence_id)

    def _calculate_presence_single(self, presence_id):
        updated = False
        for child_name, child_module in self.sub_modules.items():
            if child_name in self.presence_data[presence_id]['history']:
                if self.presence_data[presence_id]['history'][child_name]['valid'] is True:
                    self.presence_data[presence_id]['current'] = self.presence_data[presence_id]['history'][child_name]
                    updated = True
                    break

        if updated is True:
            logger.debug("Setting default presence for: {id} - {name}", id=presence_id,
                         name=self.presence_by_id[presence_id])

    def update_presence_node(self, node_id, presence_info):
        if self.upload_presence:
            self._Nodes.edit_node(node_id, {'data': json.dumps(self.presence_data[node_id])})
        else:
            data = self.presence_data[node_id].copy()
            try:
                del data['current']
            except Exception as e:
                pass
            try:
                del data['history']
            except Exception as e:
                pass
            self._Nodes.edit_node(node_id, {'data': json.dumps(self.presence_data[node_id])})

    def get_presence_id(self, requested_id):
        """
        Find the node_id (or presence_id) by either ID or name.

        :param requested_id:
        :return:
        """
        if requested_id in self.presence_by_id:
            return requested_id
        try:
            id = self.presence_by_name[requested_id]
            return id
        except Exception as e:
            raise YomboWarning("Cannot find presence object.")

    def get_presence(self, requested_id):
        """
        Get the location and are
        :param requested_id:
        :return:
        """
        requested_id = self.get_presence_id(requested_id)
        for child_name, child_module in self.sub_modules.items():
            pass

Summary
=======

For use with the [Yombo Gateway](https://yombo.net/>) automation gateway.
Provide presence information for an automation area or location. This
can be used to determine if a room, area, or building is occupied to help
with thermostat setbacks are alarm system arming.

Additional details about this module at: https://yombo.net/modules/presence

Learn about [Yombo Gateway](https://yombo.net/) or
[Get started today](https://yg2.in/start)

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download and install this module automatically.

Requirements
============

None

License
=======

The [Yombo](https://yombo.net/) team and other contributors
hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See LICENSE file for full details.
